---
hideContentTitle: true
title: About
---

### Hi there, my name is Setyo 👋

I'm a passionate fullstack developer with 10 years programming experience. I
have solid background and advanced knowledge in web development including
HTML/CSS, PHP, and Javascript/Typescript. Currently I'm working with Laravel and
Vue.js as main framework. I'm, also capable in working with database management
tools (MySQL and PostgreSQL), managing GNU/Linux server, and using DevOps tools
like Docker.
